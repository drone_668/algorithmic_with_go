package main

import (
	"algorithmic/sort/InsertSort"
	"fmt"
)

func main() {

	//values := []int{4, 93, 84, 85, 80, 37, 81, 93, 27, 12}
	values := []int{4, 5, 6, 3, 2, 1}
	fmt.Printf("Init Arry:\n%v\n", values)

	//fmt.Printf("BubbleSorted:\n%v\n", bubbleSort.BubbleSort(values))
	//fmt.Printf("BSorted2:\n%v\n", bubbleSort.BSort2(values))
	fmt.Printf("InsertSorted:\n%v\n", InsertSort.InsertSort(values))

}
