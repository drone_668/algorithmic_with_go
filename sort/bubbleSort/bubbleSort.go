package bubbleSort

// 相邻的两个元素比较，把大的往后移，每次之后，最大的元素到最后
// 每次找到最大的，放在最后
func BubbleSort(a []int) []int {

	for i := 0; i < len(a); i++ {
		//设置一个flag，当队列中某个位置的值，与之后的值比较，不需要移动位置，可以提前结束循环
		var flag bool
		for j := 0; j < len(a)-i-1; j++ {
			if a[j] > a[j+1] {
				tmp := a[j]
				a[j] = a[j+1]
				a[j+1] = tmp
				flag = true
			}
		}
		if !flag {
			break
		}

	}
	return a
}

// 从当前列队(每轮循环动态变化)，依次取出一个元素，与余下的元素比较
// 每次找出剩余中最小的，放在前面
func BSort2(values []int) []int {
	for i := 0; i < len(values)-1; i++ {

		for j := i + 1; j < len(values); j++ {
			if values[i] > values[j] {
				values[i], values[j] = values[j], values[i]
			}
		}
	}
	return values
}
